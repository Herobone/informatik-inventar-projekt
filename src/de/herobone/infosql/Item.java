package de.herobone.infosql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Objects;

public class Item {

    private String name;
    private int id;
    private int storage;
    private int subID;
    private int quantity;
    private Date date;

    public Item(String name, int id, int storage, int subID, int quantity) {

        this.id = id;
        this.name = name;
        this.storage = storage;
        this.subID = subID;
        this.quantity = quantity;

    }

    public Item(ResultSet set) throws SQLException {
        this(
                set.getString("name"),
                set.getInt("id"),
                set.getInt("STORAGE_ID"),
                set.getInt("STORAGE_SUB_ID"),
                set.getInt("qty")
        );
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStorage() {
        return storage;
    }

    public void setStorage(int storage) {
        this.storage = storage;
    }

    public int getSubID() {
        return subID;
    }

    public void setSubID(int subID) {
        this.subID = subID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return getId() == item.getId() &&
                item.getName().equals(getName());
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "(" + id + ") " + name + ": " + quantity + " [" + storage + "|" + subID + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getId(), getStorage(), getSubID(), getQuantity());
    }
}
