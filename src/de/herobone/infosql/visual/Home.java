package de.herobone.infosql.visual;

import de.herobone.infosql.Main;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Home extends Window {

    private JButton hinzufügenButton;
    private JButton ansehenButton;
    private JPanel panel;

    public Home() {
        super("Hinzufügen");
        this.setContentPane(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ansehenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(e.toString());
            }
        });
        hinzufügenButton.addActionListener(e -> Home.this.openOther(Main.addWindow));

        this.pack();
    }
}
