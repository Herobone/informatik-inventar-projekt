package de.herobone.infosql.visual;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Window extends JFrame {
    protected Window lastWindow;

    public Window(String title) {
        super(title);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                Window.this.close();
            }
        });
    }

    public void open(Window lastWindow) {
        this.lastWindow = lastWindow;
        this.setVisible(true);
    }

    public void openOther(Window other) {
        this.setVisible(false);
        other.open(this);
    }

    public void close() {
        if (this.lastWindow != null) {
            lastWindow.setVisible(true);
        }
        this.setVisible(false);
    }
}
