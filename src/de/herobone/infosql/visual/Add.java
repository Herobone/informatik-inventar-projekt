package de.herobone.infosql.visual;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Add extends Window {
    private JTextField textField1;
    private JTextField textField2;
    private JButton fertigButton;
    private JPanel panel;

    private JFrame lastWindow;

    public Add() {
        super("Hinzufügen");
        this.setContentPane(panel);
        fertigButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(textField1.getText());
                System.out.println(textField2.getText());
            }
        });

        this.pack();

    }
}
