package de.herobone.infosql;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ItemDriver {

    private final DBDriver driver;

    public ItemDriver(DBDriver driver) {
        this.driver = driver;
    }

    public ItemDriver(String url, String user, String password) throws SQLException {
        driver = new DBDriver(url, user, password);
    }

    public ArrayList<Item> getAll() throws SQLException {
        ArrayList<Item> list = new ArrayList();

        ResultSet result = this.driver.select("*", "items");

        while(result.next()) {   // Move the cursor to the next row, return false if no more row
            Item item = new Item(result);
            list.add(item);
        }

        return list;
    }

    public ArrayList<Item> get(int count) throws SQLException {
        ArrayList<Item> list = new ArrayList();

        ResultSet result = this.driver.select("*", "items", count);

        while(result.next()) {   // Move the cursor to the next row, return false if no more row
            Item item = new Item(result);
            list.add(item);
        }

        return list;
    }
}
