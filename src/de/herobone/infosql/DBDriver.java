package de.herobone.infosql;

import java.sql.*;

public class DBDriver implements AutoCloseable {

    private Connection conn;
    private Statement stmt;

    public DBDriver(String url, String user, String password) throws SQLException {
        conn = DriverManager.getConnection(url, user, password);
        stmt = conn.createStatement();
    }

    public ResultSet select(String selector, String table, String where, int limit) throws SQLException {

        String strSelect = "select " + selector + " from " + table;
        if (where != null) {
            strSelect += " where " + where;
        }

        if (limit > 0) {
            strSelect += " limit " + limit;
        }

        return stmt.executeQuery(strSelect);

    }

    public ResultSet select(String selector, String table) throws SQLException {

        ResultSet select = select(selector, table, null, -1);
        return select;

    }

    public ResultSet select(String selector, String table, int limit) throws SQLException {

        ResultSet select = select(selector, table, null, limit);
        return select;

    }

    @Override
    public void close() throws SQLException {
        conn.close();
    }
}
