package de.herobone.infosql;

import de.herobone.infosql.visual.Add;
import de.herobone.infosql.visual.Home;

import java.sql.*;
import java.util.ArrayList;

public class Main {

    public static final Add addWindow = new Add();
    public static final Home homeWindow = new Home();
    public static final Add viewWindow = new Add();

    public static void main(String[] args) {
	// write your code here
        ConfigLoader loader = new ConfigLoader();

        try (
                DBDriver driver = new DBDriver(loader.URL, loader.USERNAME, loader.PASSWORD);
            )
        {
            ItemDriver itemDriver = new ItemDriver(driver);

            ArrayList<Item> list = itemDriver.getAll();

            for (Item item : list) {
                System.out.println(item.toString());
            }

            list = itemDriver.get(2);
            for (Item item : list) {
                System.out.println(item.toString());
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        Main.homeWindow.setVisible(true);

    }
}
