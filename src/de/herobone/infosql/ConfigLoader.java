package de.herobone.infosql;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigLoader {

    public final String URL;
    public final String PASSWORD;
    public final String USERNAME;

    public ConfigLoader() {
        String url = "";
        String pass = "";
        String user = "";
        try (InputStream input = new FileInputStream("config/config.properties")) {

            Properties prop = new Properties();

            prop.load(input);

            url = prop.getProperty("db.url");
            user = prop.getProperty("db.user");
            pass = prop.getProperty("db.password");

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        this.USERNAME = user;
        this.PASSWORD = pass;
        this.URL = url;
    }
}
